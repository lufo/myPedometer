这个App实现简单的记录运动轨迹及记步功能。运行效果如下：
![](https://github.com/lufo816/myPedometer/blob/master/images/image.png)

2014-08-24更新：已实现实事定位并能画出过去一段时间的运动轨迹。

2014-08-25更新：把GPS坐标转换为百度坐标。

2014-08-26更新：1. 修复无网络时崩溃BUG。
2. 初步完成记步功能。
3. 修复开始未打开GPS时崩溃BUG。

2014-08-27更新：1. 实现GPS定位后台运行。
2. 增加无网络时提醒功能。

2014-08-28更新：加入设置计步器灵敏度功能。

2014-08-29更新：修改UI，修复部分BUG。

TODO:

优化计步器功能。

保存运动轨迹。

...