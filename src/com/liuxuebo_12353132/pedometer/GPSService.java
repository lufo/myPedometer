package com.liuxuebo_12353132.pedometer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.PolylineOptions;
import com.baidu.mapapi.model.LatLng;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class GPSService extends Service {

	private static final String TAG = "GpsActivity";

	LocationManager mlocationManager;
	private double BDLat = 0, BDLng = 0;
	Location location;
	static MyLocationData locData;
	static boolean locationChanged = true, addOverlay = false;// addOverlay记录是否要描线
	private LatLng pt1, pt2;
	static MapStatusUpdate u;
	static OverlayOptions ooPolyline;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		stopForeground(true);// 提升GPSService的优先级别
		mlocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		// 判断GPS是否正常启动
		if (!mlocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			Toast.makeText(this, "请开启GPS导航...", Toast.LENGTH_SHORT).show();
			// 返回开启GPS导航设置界面
			return;
		}
		// 监听状态
		mlocationManager.addGpsStatusListener(listener);
		// 绑定监听，有4个参数
		// 参数1，设备：有GPS_PROVIDER和NETWORK_PROVIDER两种
		// 参数2，位置信息更新周期，单位毫秒
		// 参数3，位置变化最小距离：当位置距离变化超过此值时，将更新位置信息
		// 参数4，监听
		// 备注：参数2和3，如果参数3不为0，则以参数3为准；参数3为0，则通过时间来定时更新；两者为0，则随时刷新

		// 1秒更新一次，或最小位移变化超过10米更新一次；
		// 注意：此处更新准确度非常低，推荐在service里面启动一个Thread，在run中sleep(10000);然后执行handler.sendMessage(),更新位置
		mlocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				1000, 10, locationListener);
		// 为获取地理位置信息时设置查询条件
		String bestProvider = mlocationManager.getBestProvider(getCriteria(),
				true);
		// 获取位置信息
		// 如果不设置查询要求，getLastKnownLocation方法传人的参数为LocationManager.GPS_PROVIDER
		location = mlocationManager.getLastKnownLocation(bestProvider);
		while (location == null) {
			location = mlocationManager.getLastKnownLocation(bestProvider);
			Toast.makeText(this, "GPS暂时不可用，请稍后再试...", Toast.LENGTH_SHORT)
					.show();
		}
		// updateView(location);
		// 将真实坐标转为百度坐标
		String[] point = MapFix.main(location.getLongitude(),
				location.getLatitude()).split(",");
		BDLng = Double.valueOf(point[0]).doubleValue();
		BDLat = Double.valueOf(point[1]).doubleValue();
		// 在地图上标记此时的位置
		locData = new MyLocationData.Builder().latitude(BDLat).longitude(BDLng)
				.build();
		// mBaiduMap.setMyLocationData(locData);
		// 将地图中心移到当前位置
		pt1 = new LatLng(BDLat, BDLng);
		u = MapStatusUpdateFactory.newLatLng(pt1);
		// mBaiduMap.animateMapStatus(u);
	}

	// 位置监听
	private LocationListener locationListener = new LocationListener() {

		/**
		 * 位置信息变化时触发
		 */
		public void onLocationChanged(Location location) {
			// updateView(location);
			Log.i("test", "位置改变");
			// locationChanged = true;
			// 将真实坐标转为百度坐标
			String[] point = MapFix.main(location.getLongitude(),
					location.getLatitude()).split(",");
			BDLng = Double.valueOf(point[0]).doubleValue();
			BDLat = Double.valueOf(point[1]).doubleValue();
			// 重新标记当前位置
			locData = new MyLocationData.Builder().latitude(BDLat)
					.longitude(BDLng).build();
			// mBaiduMap.setMyLocationData(locData);
			// 让当前位置始终在地图中心
			pt2 = pt1;
			pt1 = new LatLng(BDLat, BDLng);
			u = MapStatusUpdateFactory.newLatLng(pt1);
			// mBaiduMap.animateMapStatus(u);
			// 位置改变时标出轨迹
			List<LatLng> points = new ArrayList<LatLng>();
			points.add(pt1);
			points.add(pt2);
			ooPolyline = new PolylineOptions().width(5).color(0xAAFF0000)
					.points(points);
			addOverlay = true;
		}

		/**
		 * GPS状态变化时触发
		 */
		public void onStatusChanged(String provider, int status, Bundle extras) {
			switch (status) {
			// GPS状态为可见时
			case LocationProvider.AVAILABLE:
				Log.i(TAG, "当前GPS状态为可见状态");
				break;
			// GPS状态为服务区外时
			case LocationProvider.OUT_OF_SERVICE:
				Log.i(TAG, "当前GPS状态为服务区外状态");
				break;
			// GPS状态为暂停服务时
			case LocationProvider.TEMPORARILY_UNAVAILABLE:
				Log.i(TAG, "当前GPS状态为暂停服务状态");
				break;
			}
		}

		/**
		 * GPS开启时触发
		 */
		public void onProviderEnabled(String provider) {
			location = mlocationManager.getLastKnownLocation(provider);
			// updateView(location);
		}

		/**
		 * GPS禁用时触发
		 */
		public void onProviderDisabled(String provider) {
			// updateView(null);
		}

	};

	// 状态监听
	GpsStatus.Listener listener = new GpsStatus.Listener() {
		public void onGpsStatusChanged(int event) {
			switch (event) {
			// 第一次定位
			case GpsStatus.GPS_EVENT_FIRST_FIX:
				Log.i(TAG, "第一次定位");
				break;
			// 卫星状态改变
			case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
				Log.i(TAG, "卫星状态改变");
				// 获取当前状态
				GpsStatus gpsStatus = mlocationManager.getGpsStatus(null);
				// 获取卫星颗数的默认最大值
				int maxSatellites = gpsStatus.getMaxSatellites();
				// 创建一个迭代器保存所有卫星
				Iterator<GpsSatellite> iters = gpsStatus.getSatellites()
						.iterator();
				int count = 0;
				while (iters.hasNext() && count <= maxSatellites) {
					count++;
				}
				System.out.println("搜索到：" + count + "颗卫星");
				break;
			// 定位启动
			case GpsStatus.GPS_EVENT_STARTED:
				Log.i(TAG, "定位启动");
				break;
			// 定位结束
			case GpsStatus.GPS_EVENT_STOPPED:
				Log.i(TAG, "定位结束");
				break;
			}
		};
	};

	private Criteria getCriteria() {
		Criteria criteria = new Criteria();
		// 设置定位精确度 Criteria.ACCURACY_COARSE比较粗略，Criteria.ACCURACY_FINE则比较精细
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		// 设置是否要求速度
		criteria.setSpeedRequired(false);
		// 设置是否允许运营商收费
		criteria.setCostAllowed(false);
		// 设置是否需要方位信息
		criteria.setBearingRequired(false);
		// 设置是否需要海拔信息
		criteria.setAltitudeRequired(false);
		// 设置对电源的需求
		criteria.setPowerRequirement(Criteria.POWER_LOW);
		return criteria;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		locationChanged = false;// 服务停止
	}
}