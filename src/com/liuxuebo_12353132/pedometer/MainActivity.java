package com.liuxuebo_12353132.pedometer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.provider.Settings;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.PolylineOptions;
import com.baidu.mapapi.model.LatLng;

public class MainActivity extends Activity {
	MapView mMapView;
	BaiduMap mBaiduMap;
	// LocationManager mlocationManager;
	// private static final String TAG = "GpsActivity";
	// private LatLng pt1, pt2;
	private TextView tv_show_step;// 步数
	private Button btn_start;// 开始按钮
	private Button btn_stop;// 停止按钮
	private EditText inputSensitivity;
	// private double BDLat = 0, BDLng = 0;
	private Thread thread_Step = null, thread_GPS = null; // 定义线程对象
	static Context mContext; // 全局的上下文
	boolean firstTime = true, destory = false;

	// Location location;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 在使用SDK各组件之前初始化context信息，传入ApplicationContext
		mContext = getApplicationContext();
		SDKInitializer.initialize(mContext);
		setContentView(R.layout.activity_main);
		firstTime = true;
		destory = false;
		inputSensitivity = (EditText) this.findViewById(R.id.sensitiyity);
		// 防止主线程不能访问网络
		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		tv_show_step = (TextView) this.findViewById(R.id.show_step);
		tv_show_step.setText("0");
		btn_start = (Button) this.findViewById(R.id.start);
		btn_stop = (Button) this.findViewById(R.id.stop);
		// 获取地图控件引用
		mMapView = (MapView) findViewById(R.id.bmapView);
		mBaiduMap = mMapView.getMap();
		// 设置缩放级别
		mBaiduMap.setMapStatus(MapStatusUpdateFactory.zoomTo(18));
		// 开启定位图层
		mBaiduMap.setMyLocationEnabled(true);

		if (thread_GPS == null) {
			thread_GPS = new Thread() {
				@Override
				public void run() {
					super.run();
					while (true) {
						try {
							Thread.sleep(300);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						if (GPSService.locationChanged) {
							// GPSService.locationChanged = false;
							Message msg = new Message();
							handler_GPS.sendMessage(msg);// 通知主线程
						}
					}
				}
			};
			thread_GPS.start();
		}

		if (thread_Step == null) {

			thread_Step = new Thread() {// 子线程用于监听当前步数的变化

				@Override
				public void run() {
					super.run();
					while (true) {
						try {
							Thread.sleep(300);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						if (StepService.FLAG) {
							Message msg = new Message();
							handler_Step.sendMessage(msg);// 通知主线程
						}
					}
				}
			};
			thread_Step.start();
		}
		Intent service_GPS = new Intent(this, GPSService.class);
		startService(service_GPS);
	}

	Handler handler_GPS = new Handler() {// Handler对象用于更新当前步数,定时发送消息，调用方法查询数据用于显示
		// 主要接受子线程发送的数据, 并用此数据配合主线程更新UI
		// Handler运行在主线程中(UI线程中), 它与子线程可以通过Message对象来传递数据,
		// Handler就承担着接受子线程传过来的(子线程用sendMessage()方法传递Message对象，(里面包含数据)
		// 把这些消息放入主线程队列中，配合主线程进行更新UI。

		@Override
		// 这个方法是从父类/接口 继承过来的，需要重写一次
		public void handleMessage(Message msg) {
			super.handleMessage(msg); // 此处可以更新UI
			if (!destory) {// TODO:
							// 不这么写在主线程退出时会崩溃，但这样写在主线程退出后走的轨迹就不能描出来，只能等再次create后画出一条直线
				// 在地图上标记此时的位置
				mBaiduMap.setMyLocationData(GPSService.locData);
				// 将地图中心移到当前位置
				if (firstTime) {
					firstTime = false;
					mBaiduMap.animateMapStatus(GPSService.u);
				}
				// 位置改变时标出轨迹
				if (GPSService.addOverlay && GPSService.ooPolyline != null)
					mBaiduMap.addOverlay(GPSService.ooPolyline);
			}
		}
	};

	Handler handler_Step = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			tv_show_step.setText(String.valueOf(StepDetector.CURRENT_SETP));// 显示当前步数
		}
	};

	/*
	 * private void countStep() { if (StepDetector.CURRENT_SETP % 2 == 0) {
	 * total_step = StepDetector.CURRENT_SETP / 2 * 3; } else { total_step =
	 * StepDetector.CURRENT_SETP / 2 * 3 + 1; } }
	 */

	public void onClick(View view) {
		Intent service = new Intent(this, StepService.class);
		switch (view.getId()) {
		case R.id.start:
			startService(service);
			btn_start.setEnabled(false);
			btn_stop.setEnabled(true);
			btn_stop.setText(getString(R.string.pause));
			break;

		case R.id.stop:
			stopService(service);
			if (StepService.FLAG && StepDetector.CURRENT_SETP > 0) {
				btn_stop.setText(getString(R.string.cancel));
			} else {
				StepDetector.CURRENT_SETP = 0;

				btn_stop.setText(getString(R.string.pause));
				btn_stop.setEnabled(false);

				tv_show_step.setText("0");

				handler_Step.removeCallbacks(thread_Step);
			}
			btn_start.setEnabled(true);
			break;

		case R.id.confirm:
			if (inputSensitivity.getText().toString().equals(""))
				Toast.makeText(this, "请输入0-40的纯数字...", Toast.LENGTH_SHORT)
						.show();
			else {
				if (isNumeric(inputSensitivity.getText().toString())) {
					int temp = Integer.parseInt(inputSensitivity.getText()
							.toString());
					if (temp > 40 || temp < 0)
						Toast.makeText(this, "请输入0-40的纯数字...",
								Toast.LENGTH_SHORT).show();
					else
						StepDetector.sensitivity = 40 - temp;
				} else
					Toast.makeText(this, "请输入0-40的纯数字...", Toast.LENGTH_SHORT)
							.show();
			}
		}
	}

	// 利用正则表达式判断字符串是否是数字
	public static boolean isNumeric(String str) {
		Pattern pattern = Pattern.compile("[0-9]*");
		return pattern.matcher(str).matches();
	}

	@Override
	protected void onDestroy() {
		destory = true;
		super.onDestroy();
		// 在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
		mMapView.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
		// 在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
		mMapView.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		// 在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
		mMapView.onPause();
	}
}
