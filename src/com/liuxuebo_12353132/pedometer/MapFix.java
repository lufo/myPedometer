package com.liuxuebo_12353132.pedometer;

import it.sauronsoftware.base64.Base64;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.widget.Toast;
import net.sf.json.JSONObject;

public class MapFix {
	// static boolean internetAvailable = true;

	public static String main(double longitude, double latitude) {
		// 转换前的GPS坐标
		double x = 116.350241303468;
		double y = 39.960621618134;
		String lastLocation = "116.35024130346,39.960621618134";
		x = longitude;
		y = latitude;
		String path = "http://api.map.baidu.com/ag/coord/convert?from=0&to=4&x="
				+ x + "+&y=" + y + "&callback=BMap.Convertor.cbk_7594";
		try {
			// 使用http请求获取转换结果
			URL url = new URL(path);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(5 * 1000);
			InputStream inStream = conn.getInputStream();

			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = inStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, len);
			}
			// 得到返回的结果
			String res = outStream.toString();
			// 处理结果
			if (res.indexOf("(") > 0 && res.indexOf(")") > 0) {
				String str = res.substring(res.indexOf("(") + 1,
						res.indexOf(")"));
				String err = res.substring(res.indexOf("error") + 7,
						res.indexOf("error") + 8);
				if ("0".equals(err)) {
					JSONObject js = JSONObject.fromObject(str);
					// 编码转换
					String x1 = new String(Base64.decode(js.getString("x")));
					String y1 = new String(Base64.decode(js.getString("y")));
					lastLocation = x1 + "," + y1;
					// internetAvailable = true;
					return lastLocation;
				}
			}
			// internetAvailable = false;
			Toast.makeText(MainActivity.mContext, "网络暂时不可用，请稍后再试...",
					Toast.LENGTH_SHORT).show();
			return lastLocation;
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(MainActivity.mContext, "网络暂时不可用，请稍后再试...",
					Toast.LENGTH_SHORT).show();
			// internetAvailable = false;
			return lastLocation;
		}

	}
}
